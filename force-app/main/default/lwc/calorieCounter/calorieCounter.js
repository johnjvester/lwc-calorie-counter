import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getBarcodeScanner } from 'lightning/mobileCapabilities';
import findSnackByUpcEquals from '@salesforce/apex/SnackController.findSnackByUpcEquals';
import UserPreferencesRecordHomeSectionCollapseWTShown from '@salesforce/schema/User.UserPreferencesRecordHomeSectionCollapseWTShown';

export default class CalorieCounter extends LightningElement {
    myScanner;
    buttonLabel = 'Scan Barcode';
    scanButtonDisabled = false;
    resultsFound = false;
    snackFound = false;
    scannedBarcode = '';
    scannedBarcodeType = '';
    snackName = '';
    snackCalories = 0;
    snackUPC = '';
    
    // When component is initialized, detect whether to enable Scan button
    connectedCallback() {
        this.myScanner = getBarcodeScanner();
        if (this.myScanner == null || !this.myScanner.isAvailable()) {
            this.scanButtonDisabled = true;
        }
    }

    handleBeginScanClick(event) {
        // Reset scannedBarcode to empty string before starting new scan
        this.scannedBarcode = '';
        this.resultsFound = false;
        this.snackFound = false;

        // Make sure BarcodeScanner is available before trying to use it
        // Note: We _also_ disable the Scan button if there's no BarcodeScanner
        if (this?.myScanner.isAvailable()) {
            const scanningOptions = {
                barcodeTypes: [ 
                    this.myScanner.barcodeTypes.UPC_E
                ]
            };
            this.myScanner
                .beginCapture(scanningOptions)
                .then((result) => {
                    console.log('result', result);

                    this.resultsFound = true;
                    this.scannedBarcode =  result.value;
                    this.scannedBarcodeType = result.type;

                    findSnackByUpcEquals({ upcId: this.scannedBarcode })
                    .then((snack) => {
                        console.log('snack', snack);
                    
                        this.snackFound = true;
                        this.snackName = snack.Name;
                        this.snackCalories = snack.Calories__c;
                        this.snackUPC = snack.UPC__c;
                        this.error = undefined;
                        this.buttonLabel = 'Scan Another Snack Barcode';
                    })
                    .catch((error) => {
                        throw error;
                    });
                })
                .catch((error) => {
                    // Handle cancellation and unexpected errors here
                    console.error(error);
                    this.snackFound = false;
                    this.buttonLabel = 'Scan Barcode';


                    // Inform the user we ran into something unexpected
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Barcode Scanner Error',
                            message:
                                'There was a problem scanning the barcode: ' +
                                JSON.stringify(error) +
                                ' Please try again.',
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                })
                .finally(() => {
                    console.log('#finally');

                    // Clean up by ending capture,
                    // whether we completed successfully or had an error
                    this.myScanner.endCapture();
                });
        } else {
            // BarcodeScanner is not available
            // Not running on hardware with a camera, or some other context issue
            console.log(
                'Scan Barcode button should be disabled and unclickable.'
            );
            console.log('Somehow it got clicked: ');
            console.log(event);

            // Let user know they need to use a mobile phone with a camera
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Barcode Scanner Is Not Available',
                    message:
                        'Try again from the Salesforce app on a mobile device.',
                    variant: 'error'
                })
            );
        }
    }
}
