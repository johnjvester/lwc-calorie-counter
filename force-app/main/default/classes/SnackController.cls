public with sharing class SnackController {
    @AuraEnabled(cacheable=true)
    public static Snack__c findSnackByUpcEquals(String upcId) {
        System.debug('upcId=' + upcId);
        return [
            SELECT Name, Calories__c, UPC__c  
            FROM Snack__c 
            WHERE UPC__c = :upcId 
            LIMIT 1
        ];
    }
}